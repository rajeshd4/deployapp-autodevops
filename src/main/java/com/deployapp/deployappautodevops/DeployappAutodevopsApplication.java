package com.deployapp.deployappautodevops;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DeployappAutodevopsApplication {

	public static void main(String[] args) {
		SpringApplication.run(DeployappAutodevopsApplication.class, args);
	}

}
